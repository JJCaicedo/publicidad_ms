package com.example.publicidadms.models;

import org.springframework.data.annotation.Id;

public class Ad {
    @Id
    private String id;
    private String name;
    private String service;
    private String location;
    private String image;

    public Ad(String id, String name, String service, String location, String image) {
        this.id = id;
        this.name = name;
        this.service = service;
        this.location = location;
        this.image = image;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
