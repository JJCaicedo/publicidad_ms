package com.example.publicidadms.repositories;

import com.example.publicidadms.models.Ad;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;

public interface AdRepository extends MongoRepository<Ad, String> {
    @Query(value = "{'service': {$regex : ?0, $options: 'i'}}")
    List<Ad> findByServiceRegex(String service );
}
