package com.example.publicidadms.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@ControllerAdvice
@ResponseBody
public class InvalidFieldAdvice {
    @ResponseBody
    @ExceptionHandler(InvalidFieldException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    String InvalidFieldAdvice(InvalidFieldException ex){
        return ex.getMessage();
    }
}
