package com.example.publicidadms.exceptions;

public class AdNotFoundException extends RuntimeException{
    public AdNotFoundException(String message){
        super(message);
    }
}
