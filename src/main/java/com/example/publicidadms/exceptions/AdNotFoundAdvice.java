package com.example.publicidadms.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@ControllerAdvice
@ResponseBody
public class AdNotFoundAdvice {
    @ResponseBody
    @ExceptionHandler(AdNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    String AdNotFoundAdvice(AdNotFoundException ex){
        return ex.getMessage();
    }
}
