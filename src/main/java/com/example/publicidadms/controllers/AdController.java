package com.example.publicidadms.controllers;

import com.example.publicidadms.exceptions.*;
import com.example.publicidadms.models.Ad;
import com.example.publicidadms.repositories.AdRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class AdController {
    private final AdRepository adRepository;

    public AdController(AdRepository adRepository) {
        this.adRepository = adRepository;
    }

    @PostMapping("/ads")
    Ad createAd(@RequestBody Ad ad){
        if(ad == null)
            throw new InvalidFieldException("Object is required in request body");
        if(ad.getName() == null || ad.getName().isBlank())
            throw new InvalidFieldException("Name is required");
        if(ad.getService() == null || ad.getService().isBlank())
            throw new InvalidFieldException("Service is required");
        if(ad.getLocation() == null || ad.getLocation().isBlank())
            throw new InvalidFieldException("Location is required");
        if(ad.getImage() == null || ad.getImage().isBlank())
            throw new InvalidFieldException("Image URL is required");

        return adRepository.save(ad);
    }

    @PutMapping("/ads/update/{id}")
    Ad updateAd(@PathVariable String id,
                @RequestBody Ad data){
        Ad ad = adRepository.findById(id).orElse(null);

        if(ad == null)                                    
            throw new AdNotFoundException("Ad not found");
        if(data.getName() != null && !data.getName().isBlank() )
            ad.setName(data.getName());
        if(data.getLocation() != null && !data.getLocation().isBlank() )
            ad.setLocation(data.getLocation());
        if(data.getService() != null && !data.getService().isBlank() )
            ad.setService(data.getService());
        if(data.getImage() != null && !data.getImage().isBlank() )
            ad.setImage(data.getImage());

        return adRepository.save(ad);
    }

    @GetMapping("/ads")
    List<Ad> getAds(@RequestParam(required = false) String service){

        if(service != null){
            List<Ad> ads = adRepository.findByServiceRegex(service);
            return ads;
        }

        List<Ad> ads = adRepository.findAll();
        return ads;
    }

    @GetMapping("/ads/{id}")
    Ad getAd(@PathVariable String id){
        Ad ad = adRepository.findById(id).orElse(null);

        if(ad == null)
            throw new AdNotFoundException("Ad not found");

        return ad;
    }

    @DeleteMapping("/ads/delete/{id}")
    String deleteAd(@PathVariable String id){
        Ad ad = adRepository.findById(id).orElse(null);

        if(ad == null)
            throw new AdNotFoundException("Ad not found");
        
        adRepository.deleteById(id);
        return "Ad deleted";
    }
}
